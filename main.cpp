#include <iostream>

#include <boost/thread.hpp>
#include <boost/asio.hpp>

#include <websocketpp/config/asio_no_tls_client.hpp>
#include <websocketpp/client.hpp>


typedef websocketpp::client<websocketpp::config::asio_client> client;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

// pull out the type of messages sent by our config
typedef websocketpp::config::asio_client::message_type::ptr message_ptr;

// This message handler will be invoked once for each incoming message. It
// prints the message and then sends a copy of the message back to the server.
void on_message(client* c, websocketpp::connection_hdl hdl, message_ptr msg) {
    std::cout << "on_message called with hdl: " << hdl.lock().get()
              << " and message: " << msg->get_payload()
              << std::endl;


    //websocketpp::lib::error_code ec;

    //c->send(hdl, msg->get_payload(), msg->get_opcode(), ec);
    //if (ec) {
    //	std::cout << "Echo failed because: " << ec.message() << std::endl;
    //}
}

void web_run(client* c)
{
    try
    {

        c->run();
    }
    catch (const std::exception&)
    {

        c->get_alog().write(websocketpp::log::alevel::app, "RUN EXCEPTION !");
    }

    c->get_alog().write(websocketpp::log::alevel::app, "RUN ENDDDDDDD.");
}

void on_close(client* c, websocketpp::connection_hdl hdl)
{
    c->get_alog().write(websocketpp::log::alevel::app, "Connection Closed");
}

void on_open(client* c, websocketpp::connection_hdl hdl) {
    std::string msg = "Hello";
    c->send(hdl, msg, websocketpp::frame::opcode::text);
    c->get_alog().write(websocketpp::log::alevel::app, "Sent Message: " + msg);
}

int main() {
    bool done = false;
    std::string input;

    client c;
    websocketpp::connection_hdl m_hdl;
    websocketpp::session::state::value con_state;

    std::string uri = "ws://localhost:6789";

    c.init_asio();
    c.start_perpetual();

//    ws_thread.reset(new websocketpp::lib::thread(&client::run, &c));
    websocketpp::lib::thread asio_thread(&client::run, &c);

    // Register our message handler
    c.set_message_handler(bind(&on_message, &c, ::_1, ::_2));
    c.set_close_handler(bind(&on_close, &c, ::_1));
    c.set_open_handler(bind(&on_open, &c, ::_1));
    //c.set_fail_handler(bind(&on_fail, &c, ::_1));
    //c.set_message_handler(bind(&on_message, &c, ::_1, ::_2));
    //c.set_close_handler(bind(&on_close, &c, ::_1));

    websocketpp::lib::error_code ec;
    client::connection_ptr con = c.get_connection(uri, ec);
    if (ec) {
        std::cout << "could not create connection because: " << ec.message() << std::endl;
        return 0;
    }

    while (!done){
        std::cout << "Enter Command: ";
        std::getline(std::cin, input);

        if (input == "q") {
            done = true;

            c.stop_perpetual();
            c.get_io_service().stop();

            if (asio_thread.joinable()) {
                //asio_thread.join();
                asio_thread.detach();
            }

        } else if (input == "t") {
            // get status
            std::cout << "Status: " << con->get_state() << std::endl;

        } else if (input == "c") {
            con = c.get_connection(uri, ec);
            if (ec) {
                std::cout << "could not create connection because: " << ec.message() << std::endl;
                return 0;
            }

            c.connect(con);

            m_hdl = con->get_handle();

            std::cout << "Status: " << con->get_state() << std::endl;

        } else if (input == "s") {
            c.close(m_hdl, websocketpp::close::status::normal, "");
            c.reset();

            std::cout << "Status: " << con->get_state() << std::endl;

        } else if (input == "m") {
            std::string msg = "MeSSAGE";
            c.send(m_hdl, msg, websocketpp::frame::opcode::text);
        }
    }

    std::cout << "By !" << std::endl;
    return 0;
}